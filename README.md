# Ubupack - MacOS

A full theme pack to personalize Ubuntu quickly using the MacOS UI.

## What's in?
* Cursors
* Fonts
* UI shell theme
* Dark and lights packs
* Special icon pack
* A dock
* A simple installer to install everything above : )

## How to install
1. To install this you needs to start the sh file like this:

```
git clone https://gitea.com/chopin42/Ubupack-MacOS.git
cd Ubupack-MacOS
chmod +x setup.sh
sudo sh setup.sh
```

2. The biggest work is done now you needs to configure tweaks activate user-themes in extensions
3. In the appearance selects the followings: dark or light, cursor, icons, dark or light, default
4. In fonts, for the 2 first choices select SF Pro Display Regular : 12. Then for "monospace" select Monospace Bold. For the last one select Monopace Regular : 13.
5. Into startup application select "plank"
6. Restart your computer to apply all changes...

## How to uninstall
1. To uninstall this app you just needs to run the remove script:

```
git clone https://gitea.com/chopin42/Ubupack-MacOS.git
cd Ubupack-MacOS
chmod +x remove.sh
sudo sh remove.sh
```

2. In appearance selects the followings: Yaru, Yaru, Yaru, Default, Yaru
3. In fonts selects Ubuntu Regular 11, Sans Regular 11, ubuntu Mono Regular 13, Ubuntu bold 11.
4. In startup remove "plank"
5. Restart your computer
