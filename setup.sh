# Installing dependencies
sudo apt install plank
sudo apt install chrome-gnome-shell
sudo apt install gnome-tweaks

# Creating new folders
mkdir ~/.icons
mkdir ~/.themes
mkdir ~/.fonts

# Move the different folders to the right locations
cp fonts/font.otf ~/.fonts
cp -r icons ~/.icons
cp -r themes/dark ~/.themes
cp -r cursor ~/.icons

# Configure tweaks
gnome-tweaks

# Configure plank
plank --preferences

# Autohide the ubuntu dock
echo WARN: the dock cannot be hidden automatically
